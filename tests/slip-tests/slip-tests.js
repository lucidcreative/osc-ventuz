/*global slip, QUnit, deepEqual, test*/

// TODO: update all encode tests

(function () {

    "use strict";

    var slipTestSpecs = {
        encode: [
            {
                name: "plain message",
                message: [
                    105, 32, 114, 101,
                    109, 101, 109, 98,
                    101, 114, 32, 83,
                    76, 73, 80, 32,
                    102, 114, 111, 109,
                    32, 116, 104, 101,
                    32, 49, 57, 57,
                    48, 115, 0, 0
                ],
                encoded: [
                    0, 0, 0, 32,        // Ventuz packet size Int32 = 32
                    105, 32, 114, 101,
                    109, 101, 109, 98,
                    101, 114, 32, 83,
                    76, 73, 80, 32,
                    102, 114, 111, 109,
                    32, 116, 104, 101,
                    32, 49, 57, 57,
                    48, 115, 0, 0,
                ]
            },
            // Escapes are not used in the packet-sizing method that ventuz uses
            // {
            //     name: "message with inline escapes",
            //     message: [
            //         0, 105, 32, 114,
            //         101, 109, 101, 109,
            //         98, 101, 114, 32,
            //         83, 76, 73, 80,
            //         32, 102, 114, 111,
            //         109, 32, 116, 104,
            //         101, 32, 0, 49,
            //         57, 57, 48, 115,
            //         0, 0
            //     ],
            //     encoded: [
            //         0, 0, 0, 34,        // Ventuz packet size Int32 = 34
            //         0, 105, 32, 114,
            //         101, 109, 101, 109,
            //         98, 101, 114, 32,
            //         83, 76, 73, 80,
            //         32, 102, 114, 111,
            //         109, 32, 116, 104,
            //         101, 32, 0, 49,
            //         57, 57, 48, 115,
            //         0, 0, 
            //     ]
            // },
            // {
            //     name: "message with inline ends",
            //     message: [
            //         105, 32, 114,
            //         101, 109, 101, 109,
            //         98, 101, 114, 32,
            //         83, 76, 73, 80,
            //         32, 102, 114, 111,
            //         109, 32, 116, 104,
            //         101, 32, slip.END, 49,
            //         57, 57, 48, 115,
            //         0, 0
            //     ],
            //     encoded: [
            //         slip.END, 105, 32, 114,
            //         101, 109, 101, 109,
            //         98, 101, 114, 32,
            //         83, 76, 73, 80,
            //         32, 102, 114, 111,
            //         109, 32, 116, 104,
            //         101, 32, slip.ESC, slip.ESC_END, 49,
            //         57, 57, 48, 115,
            //         0, 0, slip.END
            //     ]
            // },
            // {
            //     name: "both escapes and ends, large message size",
            //     message: [
            //         slip.ESC, 105, 32, 114,
            //         101, 109, 101, 109,
            //         98, 101, 114, 32,
            //         83, 76, 73, 80,
            //         32, 102, 114, 111,
            //         109, 32, 116, 104,
            //         101, 32, slip.END, 49,
            //         57, 57, 48, 115,
            //         0, 0
            //     ],
            //     encoded: [
            //         slip.END, slip.ESC, slip.ESC_ESC, 105, 32, 114,
            //         101, 109, 101, 109,
            //         98, 101, 114, 32,
            //         83, 76, 73, 80,
            //         32, 102, 114, 111,
            //         109, 32, 116, 104,
            //         101, 32, slip.ESC, slip.ESC_END, 49,
            //         57, 57, 48, 115,
            //         0, 0, slip.END
            //     ],
            //     bufferPadding: 2
            // }
        ],

        decode: [
            {
                name: "single packet message",
                packets: [
                    [
                        0, 0, 0, 32,
                        105, 32, 114, 0,
                        101, 109, 101, 109,
                        98, 101, 114, 32,
                        83, 76, 73, 80,
                        32, 102, 114, 111,
                        109, 32, 116, 104,
                        101, 32, 49, 0,
                        57, 57, 48, 115,
                    ]
                ],
                messages: [
                    new Uint8Array([
                        105, 32, 114, 0,
                        101, 109, 101, 109,
                        98, 101, 114, 32,
                        83, 76, 73, 80,
                        32, 102, 114, 111,
                        109, 32, 116, 104,
                        101, 32, 49, 0,
                        57, 57, 48, 115,
                    ])
                ]
            },
            {
                name: "one message in multiple packets, no leading END byte",
                packets: [
                    [
                        0, 0, 0, 34,
                        0, 105, 32, 114,
                        101, 109, 101, 109,
                        98, 101, 114, 32,
                        83, 76, 73, 80,
                        32, 102, 114, 111,
                    ],
                    [
                        109, 32, 116, 104,
                        101, 32, 0, 49,
                        57, 57, 48, 115,
                        0, 0, 
                    ]
                ],
                messages: [
                    new Uint8Array([
                        0, 105, 32, 114,
                        101, 109, 101, 109,
                        98, 101, 114, 32,
                        83, 76, 73, 80,
                        32, 102, 114, 111,
                        109, 32, 116, 104,
                        101, 32, 0, 49,
                        57, 57, 48, 115,
                        0, 0
                    ])
                ]
            },
            {
                name: "two messages: the first spans both packets",
                packets: [
                    [
                        0, 0, 0, 34,        // Ventuz packet size Int32
                        0, 105, 32, 114,
                        101, 109, 101, 109,
                        98, 101, 114, 32,
                        83, 76, 73, 80,
                        32, 102, 114, 111,
                    ],
                    [
                        109, 32, 116, 104,
                        101, 32, 0, 49,
                        57, 57, 48, 115,
                        0, 0,               // End of packet - 34 bytes
                        0, 0, 0, 3,         // Ventuz packet size Int32
                        99, 97, 116         // End of packet - 3 bytes
                    ]
                ],
                messages: [
                    new Uint8Array([
                        0, 105, 32, 114,
                        101, 109, 101, 109,
                        98, 101, 114, 32,
                        83, 76, 73, 80,
                        32, 102, 114, 111,
                        109, 32, 116, 104,
                        101, 32, 0, 49,
                        57, 57, 48, 115,
                        0, 0
                    ]),
                    new Uint8Array([
                        99, 97, 116
                    ])
                ]
            }
        ]
    };

    var tests = {
        encode: function (testSpec) {
            test(testSpec.name, function () {
                var actual = slip.encode(testSpec.message, {
                    bufferPadding: testSpec.bufferPadding
                });

                // TODO: This will likely fail in Node.js due to their TypedArray implementation.
                deepEqual(actual, new Uint8Array(testSpec.encoded),
                    "The message should be correctly SLIP encoded.");
            });
        },

        decode: function (testSpec) {
            test(testSpec.name, function () {
                var messages = [];

                var callback = function (msg) {
                    messages.push(msg);
                };

                var decoder = new slip.Decoder(callback);

                for (var i = 0; i < testSpec.packets.length; i++) {
                    decoder.decode(testSpec.packets[i]);
                }

                deepEqual(messages, testSpec.messages,
                    "The messages should have been decoded correctly.");
            });
        }
    };

    var runTests = function (testSpecs) {
        for (var testType in testSpecs) {
            var testSpecsForType = testSpecs[testType];

            QUnit.module(testType);

            for (var i = 0; i < testSpecsForType.length; i++) {
                var testSpec = testSpecsForType[i];
                tests[testType](testSpec);
            }
        }
    };

    runTests(slipTestSpecs);
}());
